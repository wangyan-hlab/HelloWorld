// 测试array容器，array容器元素个数固定、不能改变
#include <iostream>
#include <array>
// using namespace std;

void begin_end_test();
void rbegin_rend_test();
void visit();

int main()
{
    std::array<double, 10> values{0.5,1.0,1.5,2.0}; // 只初始化前4个值
    // std::array<double, 10>::iterator i;

    std::cout << std::noboolalpha << values.empty() << "==" 
        << std::boolalpha << values.empty() << std::endl;

    if (!values.empty())
    {
        for (auto i = values.begin(); i != values.end(); i++)
            std::cout << *i << " ";
    }

    std::cout << values.front() <<std::endl;

    double total = 0;
    for (auto&& value : values)
        total += value;
    std::cout << "Sum of all elements in the container: " << total << std::endl;

    begin_end_test();
    std::cout << "\n";
    rbegin_rend_test();
    std::cout << "\n";
    visit();
    
    return 0;
}

void begin_end_test()
{
    std::array<double, 5> values2;
    
    if (values2.empty())
        std::cout << "The container is empty!" << std::endl;
    else
        std::cout << "The container has " <<  values2.size() << " elements." << std::endl;

    int h = 1;
    auto first = values2.begin();   
    // 若用cbegin()，则为const类型， 不能修改元素
    // *first = 10; 这样的操作不被允许
    auto last = values2.end();      
    // 若用cend()，则为const类型， 不能修改元素

    while (first != last)
    {
        *first = h;
        ++first;
        h++;
    }

    first = values2.begin();
    while (first != last)
    {
        std::cout << *first << " ";
        ++first;
    }
}

void rbegin_rend_test()
{
    std::array<int, 5> values;
    int h = 1;
    auto first = values.rbegin();
    auto last = values.rend();

    while (first != last)
    {
        *first = h;
        ++first;
        h++;
    }

    auto new_first = values.begin();
    auto new_last = values.end();
    while (new_first != new_last)
    {
        std::cout << *new_first << " ";
        ++new_first;
    }
}

void visit()
{
    std::array<double, 10> values{0.5,1.0,1.5,2.0}; // 只初始化前4个值
    // array容器包含了 at() 这样的成员函数，使得操作元素时比普通数组更安全
    std::cout << "使用at()成员函数可有效避免越界访问,values[1]=" << values.at(1) << std::endl;
    std::cout << "使用get<n>模板函数获取容器第n个元素,values[3]=" << std::get<3>(values) << std::endl;
    std::cout << "标准数组访问元素方式,values[2]=" << values[2] << std::endl;
}