#include <iostream>
#include <deque>
#include <array>

void create_deque();
void visit_elements();

int main()
{
    std::deque<int> d;
    d.push_back(1);     // {1}
    d.push_back(2);     // {1,2}
    d.push_back(3);     // {1,2,3}
    d.push_front(0);    //{0,1,2,3}
    std::cout << "元素个数为:" << d.size() << std::endl;

    for (auto i = d.begin(); i < d.end(); i++)
    {
        std::cout << *i << " ";
    }
    std::cout << std::endl;

    visit_elements();

    return 0;
}

void create_deque()
{
    std::deque<int> d1;
    
    std::deque<int> d2(10);
    
    std::deque<int> d3(10, 5);
    
    std::deque<int> d4(5);
    std::deque<int> d5(d4);

    int a[] = {1,2,3,4,5};
    std::deque<int> d6(a, a+5);

    std::array<int, 5> arr{11,12,13,14,15};
    std::deque<int> d7(arr.begin()+2, arr.end());
}

void visit_elements()
{
    std::deque<int>d{ 1,2,3,4 };
    std::cout << d[1] << std::endl; // {1,2,3,4}
    //修改指定下标位置处的元素
    d[1] = 5;
    std::cout << d[1] << std::endl; // {1,5,3,4}

    std::cout << d.at(1) << std::endl;
    d.at(1) = 10;
    std::cout << d.at(1) << std::endl;  // {1,10,3,4}
    //下面这条语句会抛出 out_of_range 异常
    // std::cout << d.at(10) << std::endl;

    std::cout << "deque 首元素为：" << d.front() << std::endl;
    std::cout << "deque 尾元素为：" << d.back() << std::endl;
    //修改首元素
    d.front() = 10; // {10,10,3,4}
    std::cout << "deque 新的首元素为：" << d.front() << std::endl;
    //修改尾元素
    d.back() = 20;  // {10,10,3,20}
    std::cout << "deque 新的尾元素为：" << d.back() << std::endl;

    for (auto i = d.begin(); i < d.end(); i++)
        std::cout << *i << " ";
    std::cout << std::endl;
}

// 用push_back()、emplace_back()、push_front()、emplace_front()、insert()、emplace()等添加元素
// 用pop_back()、pop_front()、erase()、clear()等删除元素