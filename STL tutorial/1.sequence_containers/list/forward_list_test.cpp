#include <iostream>
#include <forward_list>
// 效率高是选用 forward_list 而弃用 list 容器最主要的原因
// 换句话说，只要是 list 容器和 forward_list 容器都能实现的操作，应优先选择 forward_list 容器
#include <iterator>
//forward_list 不提供 size() 函数，因此使用 <iterator> 中的 distance() 函数获取元素个数

int main()
{
    std::forward_list<int> values{1,2,3};
    values.emplace_front(4);
    values.emplace_after(values.before_begin(), 5);
    values.reverse();
    for (auto it = values.begin(); it != values.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    
    // 获取forward_list容器中元素个数
    int count = std::distance(values.begin(), values.end());
    std::cout << count << std::endl;

    auto it = values.begin();
    std::advance(it, 2);    // 移动迭代器
    while (it != values.end())
    {
        std::cout << *it << " ";
        ++it;
    }

    return 0;
}
