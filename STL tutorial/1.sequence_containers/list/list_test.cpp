#include <iostream>
#include <list>
#include <array>

void visit_elements();
void add_elements();

int main()
{
    std::list<int> demo{1,2,3,4,5};
    // list迭代器是双向迭代器，不是随机访问迭代器
    // 因此不能使用下标a[i]访问指定位置的元素
    // 也不能使用 -=、+=、+、-运算符，以及<、>、<=、>=比较运算符
    for (auto i = demo.begin(); i != demo.end(); i++)
        std::cout << *i << " ";
    std::cout << std::endl;
    for (auto j = demo.rbegin(); j != demo.rend(); j++)
        std::cout << *j << " ";
    std::cout << std::endl;

    visit_elements();
    add_elements();

    return 0;
}

// 值得一提的是，list 容器在进行插入（insert()）、接合（splice()）等操作时，都不会造成原有的 list 迭代器失效，甚至进行删除操作，而只有指向被删除元素的迭代器失效，其他迭代器不受任何影响。
// 在进行插入操作之后，仍使用先前创建的迭代器遍历容器，虽然程序不会出错，但由于插入位置的不同，可能会遗漏新插入的元素

void visit_elements()
{
    std::list<int> mylist{1,2,3,4};
    // 使用front()和back()函数访问和修改首尾元素值
    int &first = mylist.front();
    int &last = mylist.back();
    std::cout << first << " " << last << std::endl;
    first = 10;
    last = 20;
    std::cout << mylist.front() << " " << mylist.back() << std::endl;
    // 使用迭代器访问元素
    auto it = mylist.begin();
    std::cout << *it << "\n";
    ++it;
    while (it != mylist.end())
    {
        std::cout << *it << " ";
        ++it;
    }
    std::cout << "\n";
}

void add_elements()
{
    std::list<int> values{1,2,3};
    values.push_front(0);//{0,1,2,3}
    values.push_back(4); //{0,1,2,3,4}
    values.emplace_front(-1);//{-1,0,1,2,3,4}
    values.emplace_back(5);  //{-1,0,1,2,3,4,5}
   
    //emplace(pos,value),其中 pos 表示指明位置的迭代器，value为要插入的元素值
    values.emplace(values.end(), 6);//{-1,0,1,2,3,4,5,6}
    for (auto p = values.begin(); p != values.end(); ++p) {
        std::cout << *p << " ";
    }
    std::cout << "\n";

    // insert()函数用法
    std::list<int> values1{ 1,2 };
    //第一种格式用法
    values1.insert(values1.begin(), 3);//{3,1,2}
    //第二种格式用法
    values1.insert(values1.end(), 2, 5);//{3,1,2,5,5}
    //第三种格式用法
    std::array<int, 3>test{ 7,8,9 };
    values1.insert(values1.end(), test.begin(), test.end());//{3,1,2,5,5,7,8,9}
    //第四种格式用法
    values1.insert(values1.end(), { 10,11 });//{3,1,2,5,5,7,8,9,10,11}
    for (auto p = values1.begin(); p != values1.end(); ++p)
    {
        std::cout << *p << " ";
    }

    //splice()函数用法
    //创建并初始化 2 个 list 容器
    std::list<int> mylist1{ 1,2,3,4 }, mylist2{10,20,30};
    std::list<int>::iterator it = ++mylist1.begin(); //指向 mylist1 容器中的元素 2
   
    //调用第一种语法格式
    mylist1.splice(it, mylist2); // mylist1: 1 10 20 30 2 3 4
                                 // mylist2:
                                 // it 迭代器仍然指向元素 2，只不过容器变为了 mylist1
    //调用第二种语法格式，将 it 指向的元素 2 移动到 mylist2.begin() 位置处
    mylist2.splice(mylist2.begin(), mylist1, it);   // mylist1: 1 10 20 30 3 4
                                                    // mylist2: 2
                                                    // it 仍然指向元素 2
   
    //调用第三种语法格式，将 [mylist1.begin(),mylist1.end())范围内的元素移动到 mylist.begin() 位置处                  
    mylist2.splice(mylist2.begin(), mylist1, mylist1.begin(), mylist1.end());//mylist1:
                                                                             //mylist2:1 10 20 30 3 4 2
   
    std::cout << "mylist1 包含 " << mylist1.size() << "个元素" << std::endl;
    std::cout << "mylist2 包含 " << mylist2.size() << "个元素" << std::endl;
    //输出 mylist2 容器中存储的数据
    std::cout << "mylist2:";
    for (auto iter = mylist2.begin(); iter != mylist2.end(); ++iter) {
        std::cout << *iter << " ";
    }
}