#include <iostream>
#include <vector>
#include <array>
#include <algorithm>    // 为了引入swap()和remove()函数

void create_vector();
void pushback_emplaceback();
void insert_emplace();
void delete_elements();

int main()
{
    std::vector <int> a;
    a.reserve(20);  //设置容器的内存分配，至少可容纳20个元素
                    //若此前容量已经不少于20个元素，则该条语句无效
                    //调用reserve()还可能导致之前创建的迭代器失效，后面最好重新生成一下

    for (int i = 0; i < 10; i++)
    {
        a.push_back(i);
    }
    // resize a to 20
    a.resize(100);
    std::cout << a[8] << " " << a[90] << std::endl;
    a[90] = 100;
    std::cout << a.size() << " " << a[90] << std::endl;
    // delete all elements in a 
    a.clear();
    // resize a to 20 and save twenty -1 in it
    a.resize(20, -1);
    a[11] = 11;
    a.pop_back();
    std::cout << a.size() << " "<< a[18] << " " << a.at(11) << std::endl;
    // return the pointer to a[0]
    std::cout << a.data();

    pushback_emplaceback();
    insert_emplace();
    delete_elements();

    return 0;
}

void create_vector()
{
    //可以在创建同时指定初始值和元素个数
    std::vector<int> primes{2,3,5,7,11,13,17,19};
    //元素个数+初始值，这种方法所有元素值都为相同初始值
    std::vector<double> values(20, 1.0);
    //还可使用变量表示上述参数
    int num = 20;
    double value = 1.0;
    std::vector<double> values0(num, value);
    //还可通过存储元素类型相同的其他vector容器创建新的vector容器
    std::vector<char> value1(5, 'c');
    std::vector<char> value2(value1);
    //如果不想复制其他容器中的所有元素，可以用一对指针或迭代器来指定初始值的范围
    int arr[] = {1,2,3};
    std::vector<int> values1(arr,arr+2); //values将保存{1，2}
    std::vector<int> value3{1,2,3,4,5};
    std::vector<int> value4(std::begin(value3), std::begin(value3)+3);  //value2将保存{1,2,3}

}

void pushback_emplaceback()
{
    std::vector<int> values{};
    //emplace_back()直接在容器尾部创建元素
    //和push_back()相比，省去了拷贝或移动元素的过程，执行效率更高，故应优先选用
    values.emplace_back(1);
    values.emplace_back(2);
    std::cout << "\n";
    // for (int i = 0; i < values.size(); i++) {
    //     std::cout << values[i] << " ";
    // }
    for (auto i = values.begin(); i != values.end(); i++)
        std::cout << *i << " ";
    std::cout << "\n";
}

void insert_emplace()
{
    std::vector<int> demo{1,2};
    //第一种格式用法
    demo.insert(demo.begin() + 1, 3);//{1,3,2}
    //第二种格式用法
    demo.insert(demo.end(), 2, 5);//{1,3,2,5,5}
    //第三种格式用法
    std::array<int,3>test{ 7,8,9 };
    demo.insert(demo.end(), test.begin(), test.end());//{1,3,2,5,5,7,8,9}
    //第四种格式用法
    demo.insert(demo.end(), { 10,11 });//{1,3,2,5,5,7,8,9,10,11}
    for (int i = 0; i < demo.size(); i++) {
        std::cout << demo[i] << " ";
    }
    std::cout << "\n";

    std::vector<int> demo1{1,2};
    //emplace() 每次只能插入一个 int 类型元素
    demo1.emplace(demo1.begin(), 3);
    for (int i = 0; i < demo1.size(); i++) {
        std::cout << demo1[i] << " ";
    }
    std::cout << "\n";
}

void delete_elements()
{
    std::vector<int> va{1,2,3,3,4,5,5,6,7,7,8,9,0};
    // pop_back()删除容器中最后一个元素
    va.pop_back();          // {1 2 3 3 4 5 5 6 7 7 8 9 }
    for (int i = 0; i < va.size(); i++)
    {
        std::cout << va[i] << " ";
    }
    std::cout << "\n";
    // erase()删除容器中指定元素
    va.erase(va.begin()+1); // {1 3 3 4 5 5 6 7 7 8 9}
    for (int i = 0; i < va.size(); i++)
    {
        std::cout << va[i] << " ";
    }
    std::cout << "\n";
    // swap()将删除目标和最后一个元素交换位置，然后用pop_back()删除
    std::swap(*(std::begin(va)+1),*(std::end(va)-1)); // {1 9 3 4 5 5 6 7 7 8 3}
    va.pop_back();  // {1 9 3 4 5 5 6 7 7 8}
    for (int i = 0; i < va.size(); i++)
    {
        std::cout << va[i] << " ";
    }
    std::cout << "\n";
    // erase()删除指定区间[a,b)中元素
    auto iter = va.erase(va.begin()+1, va.end()-3); // {1 7 7 8}
    for (int i = 0; i < va.size(); i++)
    {
        std::cout << va[i] << " ";
    }
    std::cout << "\n";
    std::vector<int> demo{1,3,3,4,3,5};
    // remove()删除容器中所有和指定元素值相等的元素
    // 返回指向最后一个元素下一个位置的迭代器，不会改变容器的大小和容量
    auto iter1 = std::remove(demo.begin(), demo.end(), 3);
    // 若仍按之前方法遍历，则返回结果不正确
    for (int i = 0; i < demo.size(); i++)
    {
        std::cout << demo[i] << " ";    // {1 4 5 4 3 5}
    }
    std::cout << "\n";
    // 执行完remove()函数后，由于容器大小和容量均未改变，
    // 因此需要借助返回的迭代器完成正确的遍历
    for (auto first=demo.begin(); first < iter1; first++)
    {
        std::cout << *first << " ";     // {1 4 5}
    }
    std::cout << "\n";
    // 将remove()和erase()搭配使用，删除多余元素
    demo.erase(iter1, demo.end());
    for (int i = 0; i < demo.size(); i++)
    {
        std::cout << demo[i] << " ";    // {1 4 5 4 3 5}
    }
    std::cout << "\n";
    // clear()删除容器中所有元素
    demo.clear();
    std::cout << "size is: " << demo.size() << std::endl;
    std::cout << "capacity is: " << demo.capacity() << std::endl;
}