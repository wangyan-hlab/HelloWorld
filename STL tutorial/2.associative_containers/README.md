# 关联式容器

## 分类
- *map*
- *set*
    - set 容器和 map、multimap 容器不同，使用 set 容器存储的各个键值对，要求键 key 和值 value 必须相等。
- *multimap*
    - multimap 容器和 map 容器的区别在于，multimap 容器中可以同时存储多（≥2）个键相同的键值对。
- *multiset*
    - multiset 容器和 set 容器的区别在于，multiset 容器中可以同时存储多（≥2）个相同的值。
