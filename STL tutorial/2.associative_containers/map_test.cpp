#include <iostream>
#include <map>

int main()
{
    // 默认键值升序
    std::map<std::string, int> myMap{{"A",10},{"B",20}};
    // 同上
    std::map<std::string, int, std::less<std::string>> newMap1{{"A",10},{"B",20}};
    //改为键值降序
    std::map<std::string, int, std::greater<std::string>> newMap2{{"A",10},{"B",20}};
    
    myMap.emplace("C",30);
    std::cout << "---" << std::endl;
    for (auto i = myMap.begin(); i != myMap.end(); ++i)
    {
        std::cout << i->first << " " << i->second << std::endl;
    } 

    // 获取键对应值的几种方法
    std::cout << "---" << std::endl;
    for (auto i = myMap.begin(); i != myMap.end(); ++i)
    {
        if (!i->first.compare("C"))
            std::cout << i->first << " " << i->second << std::endl;
    }    

    std::cout << "B" << " " << myMap.at("B") << std::endl;  // 推荐使用

    auto myIter = myMap.find("A");
    std::cout << myIter->first << " " << myIter->second << std::endl;

    // 注意，和 insert() 方法一样，虽然 emplace_hint() 方法指定了插入键值对的位置
    // 但 map 容器为了保持存储键值对的有序状态，可能会移动其位置。
    auto iter = myMap.emplace_hint(myMap.begin(),"D",40);
    std::cout << iter->first << " " << iter->second << std::endl;

    auto iter1 = myMap.emplace_hint(myMap.begin(),"B",20);
    std::cout << iter1->first << " " << iter1->second << std::endl;

    std::cout << "---" << std::endl;
    for (auto i = myMap.begin(); i != myMap.end(); ++i)
    {
        std::cout << i->first << " " << i->second << std::endl;
    }   

    return 0;
}
