#include <iostream>
#include <map>

int main()
{
    // 创建并初始化 multimap 容器
    std::multimap<std::string, std::string>mymultimap{ 
        {"C语言教程", "http://c.biancheng.net/c/"},
        {"Python教程", "http://c.biancheng.net/python/"},
        {"STL教程", "http://c.biancheng.net/stl/"} };

    // 从已有 multimap 容器中，选定某块区域内的所有键值对，用作初始化新 multimap 容器时使用
    std::multimap<std::string, std::string>newmultimap(++mymultimap.begin(), mymultimap.end());
    for (auto it = newmultimap.begin(); it != newmultimap.end(); ++it)
        std::cout << it->first << " " << it->second << std::endl;

    //创建并初始化 multimap 容器
    std::multimap<char, int>mymultimap1{ {'a',10},{'b',20},{'b',15}, {'c',30} };
    //输出 mymultimap 容器存储键值对的数量
    std::cout << mymultimap1.size() << std::endl;
    //输出 mymultimap 容器中存储键为 'b' 的键值对的数量
    std::cout << mymultimap1.count('b') << std::endl;
    for (auto iter = mymultimap1.begin(); iter != mymultimap1.end(); ++iter) {
        std::cout << iter->first << " " << iter->second << std::endl;
    }

    return 0;
}
