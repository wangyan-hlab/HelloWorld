#include <iostream>
#include <set>

std::multiset<std::string> retMultiset() 
{
    std::multiset<std::string> tempmultiset{ "java","stl","python" };
    return tempmultiset;
}

int main()
{
    std::multiset<std::string> mymultiset{ "java","stl","python" };
    std::multiset<std::string> copymultiset(mymultiset);
    //等同于
    //std::multiset<std::string> copymultiset = mymultiset;     
    for (auto iter = copymultiset.begin(); iter != copymultiset.end(); ++iter) 
    {
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    std::multiset<std::string> copymultiset1(retMultiset());
    //等同于
    //std::multiset<std::string> copymultiset = retMultiset();
    for (auto iter = copymultiset1.begin(); iter != copymultiset1.end(); ++iter) 
    {
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    //取已有 multiset 容器中的部分元素，来初始化新 multiset 容器
    std::set<std::string> copymultiset2(++mymultiset.begin(), mymultiset.end());
    for (auto iter = copymultiset2.begin(); iter != copymultiset2.end(); ++iter) 
    {
        std::cout << *iter << " ";
    }

    return 0;
}