#include <iostream>
#include <utility>

int main()
{
    // 默认构造函数，即创建空的 pair 对象
    std::pair <std::string, double> pair1;
    // 直接使用 2 个元素初始化成 pair 对象
    std::pair <std::string, std::string> pair2("url","https://baidu.com");
    // 拷贝（复制）构造函数，即借助另一个 pair 对象，创建新的 pair 对象
    std::pair <std::string, std::string> pair3(pair2);
    // 移动构造函数
    std::pair <std::string, std::string> pair4(std::make_pair("url","https://baidu.com"));
    // 使用右值引用参数，创建 pair 对象
    std::pair <std::string, std::string> pair5(std::string("url"),std::string("https://baidu.com"));

    pair2.first = "Java教程";
    pair2.second = "http://c.biancheng.net/java/";
    std::cout << "new pair1: " << pair1.first << " " << pair1.second << std::endl;

    if (pair3 != pair2) {
        std::cout << "pair3 != pair2" << std::endl;
    }

    std::cout << "pair1: " << pair1.first << " " << pair1.second << std::endl;
    std::cout << "pair2: "<< pair2.first << " " << pair2.second << std::endl;
    std::cout << "pair3: " << pair3.first << " " << pair3.second << std::endl;
    std::cout << "pair4: " << pair4.first << " " << pair4.second << std::endl;
    std::cout << "pair5: " << pair5.first << " " << pair5.second << std::endl;

    pair3.swap(pair2);
    std::cout << "pair3: " << pair3.first << " " << pair3.second << std::endl;
    std::cout << "pair2: " << pair2.first << " " << pair2.second << std::endl;

    return 0;
}