#include <iostream>
#include <set>

void set_insert();
void set_emplace();

int main()
{
    // 创建空 set 容器
    std::set<std::string> myset;
    std::cout << "1. myset size = " << myset.size() << std::endl;
    // 向 myset 容器中插入新元素
    myset.insert("java");
    myset.insert("go");
    myset.insert("python");
    myset.insert("ruby");
    myset.insert("c");
    std::cout << "2. myset size = " << myset.size() << std::endl;

    for (auto iter = myset.begin(); iter != myset.end(); ++iter)
    {
        std::cout << *iter << std::endl;
    }
    std::cout << "\n";
    std::set<std::string>::iterator iter1 = myset.find("java");
    for (; iter1 != myset.end(); ++iter1)
    {
        std:: cout << *iter1 << std::endl;
    }
    std::cout << "\n";
    set_insert();
    set_emplace();

    return 0;
}

void set_insert()
{
    /*
        2 种语法格式的 insert() 方法，返回的都是 pair 类型的值，其包含 2 个数据，一个迭代器和一个 bool 值
        当向 set 容器添加元素成功时，该迭代器指向 set 容器新添加的元素，bool 类型的值为 true
        如果添加失败，即证明原 set 容器中已存有相同的元素，此时返回的迭代器就指向容器中相同的此元素，同时 bool 类型的值为 false
    */
        
    std::set<std::string> myset;
    std::pair<std::set<std::string>::iterator, bool> retpair;
    // 采用普通引用传值方式
    std::string str = "stl";
    retpair = myset.insert(str);
    std::cout << "iter->" << *(retpair.first) << " " 
            << "bool = " << retpair.second << std::endl;
    // 采用右值引用传值方式
    retpair = myset.insert("python");
    std::cout << "iter->" << *(retpair.first) << " "
            << "bool = " << retpair.second << std::endl;
    
    for (auto iter = myset.begin(); iter != myset.end(); ++iter)
    {
        std::cout << *iter << std::endl;
    }

}

void set_emplace()
{
    std::set<std::string> myset;
    std::pair<std::set<std::string>::iterator, bool> ret = myset.emplace("stl");
    std::cout << "myset size = " << myset.size() << std::endl;
    std::cout << "ret.iter = <" << *(ret.first) << ", " << ret.second << ">" << std::endl;

    std::set<std::string>::iterator iter = myset.emplace_hint(myset.begin(), "python");
    std::cout << "myset size = " << myset.size() << std::endl;
    std::cout << *iter << std::endl;
    
    for (auto iter1 = myset.rbegin(); iter1 != myset.rend(); ++iter1)
    {
        std::cout << "mark " << *iter1 << std::endl;
    }

}