#include <iostream>
// #include <string>
#include <unordered_map>
#include <unordered_set>

int main()
{
    std::unordered_map<std::string, std::string> myumap{
        {"C tutorial", "http://c.biancheng.net/c/"},
        {"Python tutorial", "http://c.biancheng.net/python/"},
        {"Python tutorial", "http://c.biancheng.net/python/"},
        {"Java tutorial", "http://c.biancheng.net/java/"}
    };
    std::string str = myumap.at("C tutorial");
    std::cout << "str = " << str << std::endl;

    for (auto iter = myumap.begin(); iter != myumap.end(); ++iter)
    {
        std::cout << iter->first << " " << iter->second << std::endl;
    }
    std::cout <<std::endl;

    std::unordered_multimap<std::string, std::string> myummap{
        {"Python tutorial","python"},
        {"Java tutorial", "java"},
        {"Java tutorial", "java"},
        {"Linux tutorial", "linux"}
    };

    for (auto iter = myummap.begin(); iter != myummap.end(); ++iter)
    {
        std::cout << iter->first << " " << iter->second << std::endl;
    }
    std::cout <<std::endl;

    std::unordered_set<std::string> uset{"c", "java", "java", "linux"};
    for (auto iter = uset.begin(); iter != uset.end(); ++iter)
    {
        std::cout << *iter << std::endl;
    }
    std::cout <<std::endl;

    std::unordered_multiset<std::string> umset{"c", "java", "java", "python"};
    for (auto iter = umset.begin(); iter != umset.end(); ++iter)
    {
        std::cout << *iter << std::endl;
    }
    std::cout <<std::endl;

    return 0;
}