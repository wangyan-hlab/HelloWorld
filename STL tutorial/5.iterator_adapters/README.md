# 迭代器适配器

## 分类
- 反向迭代器 *reverse_iterator*
- 安插型迭代器 *insert_iterator*
- 流迭代器 *istream_iterator / ostream_iterator*, 流缓冲区迭代器 *istreambuf_iterator / ostreambuf_iterator*
- 移动迭代器 *move_iterator*

## 辅助函数
- advance(it, n)
    - 移动的是源迭代器
- distance(first, last)
- begin(cont), end(cont)
- prev(it), next(it)
    - 不移动 it 迭代器本身，而仅仅是在 it 迭代器的基础上，得到一个移动指定位置的新迭代器
