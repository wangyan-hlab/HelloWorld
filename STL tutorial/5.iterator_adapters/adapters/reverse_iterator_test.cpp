#include <iostream>
#include <iterator>
#include <vector>

int main()
{
    std::vector<int> myvector{1,2,3,4,5,6,7,8};
    std::reverse_iterator<std::vector<int>::iterator> my_reiter(myvector.rbegin());

    // 通过重载的 * 运算符，输出其指向的元素 8
    std::cout << *my_reiter << std::endl;       // 8
    // 通过重载的 + 运算符，输出了距离当前指向位置为 3 的元素 5
    std::cout << *(my_reiter + 3) << std::endl; // 5
    // 通过重载的前置 ++ 运算符，将反向迭代器前移了 1 位，即指向了元素 7，并将其输出
    std::cout << *(++my_reiter) << std::endl;   // 7
    // 通过重载的 [ ] 运算符，输出了距离当前位置为 4 的元素 3
    std::cout << my_reiter[4] << std::endl;     // 3

    //创建并初始化反向迭代器 begin，其指向元素 1 之前的位置
    std::reverse_iterator<std::vector<int>::iterator> begin(myvector.begin());
    //创建并初始化反向迭代器 begin，其指向元素 8
    std::reverse_iterator<std::vector<int>::iterator> end(myvector.end());
    //begin底层基础迭代器指向元素 1，end底层基础迭代器指向元素 8 之后的位置
    for (auto iter = begin.base(); iter != end.base(); ++iter) {
        std::cout << *iter << ' ';
    }

    return 0;
}