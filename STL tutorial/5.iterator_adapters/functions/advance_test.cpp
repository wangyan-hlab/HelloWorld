#include <iostream>     // std::cout
#include <iterator>     // std::advance
#include <forward_list>
#include <vector>
using namespace std;

int main() {
    //创建一个 forward_list 容器
    forward_list<int> mylist{1,2,3,4};
    //it为前向迭代器，其指向 mylist 容器中第一个元素
    forward_list<int>::iterator it = mylist.begin();
    //借助 advance() 函数将 it 迭代器前进 2 个位置
    advance(it, 2);
    cout << "*it = " << *it << endl;

    //创建一个 vector 容器
    vector<int> myvector{1,2,3,4};
    //it1为随机访问迭代器，其指向 myvector 容器中第一个元素
    vector<int>::iterator it1 = myvector.begin();
    //借助 advance() 函数将 it1 迭代器前进 2 个位置
    advance(it1, 2);
    cout << "1、*it1 = " << *it1 << endl;
    //继续使用it，其指向 myvector 容器中最后一个元素之后的位置
    it1 = myvector.end();
    //借助 advance() 函数将 it1 迭代器后退 3 个位置
    advance(it1, -3);
    cout << "2、*it1 = " << *it1;

    return 0;
}
// 注意，advance() 函数本身不会检测 it 迭代器移动 n 个位置的可行性，
// 如果 it 迭代器的移动位置超出了合理范围，it 迭代器的指向将无法保证，
// 此时使用 *it 将会导致程序崩溃。