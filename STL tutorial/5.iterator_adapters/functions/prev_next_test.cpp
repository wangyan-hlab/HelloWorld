#include <iostream>     // std::cout
#include <iterator>     // std::next
#include <list>         // std::list

int main()
{
    std::list<int> mylist{1,2,3,4,5};
    std::list<int>::iterator it = mylist.end();
    auto newit = std::prev(it, 2);
    std::cout << "prev(it, 2) = " << *newit << std::endl;

    it = mylist.begin();
    newit = std::prev(it, -2);
    std::cout << "prev(it, -2) = " << *newit << std::endl;

    it = mylist.begin();
    newit = std::next(it, 2);
    std::cout << "next(it, 2) = " << *newit << std::endl;
    
    it = mylist.end();
    newit = std::next(it, -2);
    std::cout << "next(it, -2) = " << *newit << std::endl;

    return 0;
}