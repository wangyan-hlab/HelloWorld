#include <iostream>

class MyClass
{
    public:
        MyClass()
        {
            std::cout << "I am a constructor." << std::endl;
        }
        void myMethod()
        {
            std::cout << "Hello world!\n";
        }
        void myMethodOut();
        int x;
        void setY(int y_value)
        {
            y = y_value;
        }
        int getY()
        {
            return y;
        }
    private:
        int y;
};

// Define a method outside the class
void MyClass::myMethodOut()
{
    std::cout << "Hello world outside!\n";
}

int main()
{
    MyClass myObj;
    myObj.myMethod();
    myObj.myMethodOut();

    myObj.x = 25;
    std::cout << myObj.x << std::endl;
    myObj.setY(35);
    std::cout << myObj.getY() << std::endl;
    return 0;
}