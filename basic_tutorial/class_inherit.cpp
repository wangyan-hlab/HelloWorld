#include <iostream>
// Base class
class Vehicle
{
    public:
    std::string brand = "Ford";
    void honk()
    {
        std::cout << "Tuut, tuut! \n";
    }
};
// Derived class (child)
class ChildCar: public Vehicle
{
    public:
        std::string model = "Mustang";
};
// Derived class (grandchild), Multilevel Inheritance
class GrandChildCar1: public ChildCar
{
    public:
        std::string submodel1 = "ModelX";
};

class GrandChildCar2: public ChildCar
{
    public:
        std::string submodel2 = "ModelY";
};
//Multiple Inheritance
class HybridCar: public GrandChildCar1, public GrandChildCar2
{
};

int main()
{
    ChildCar mycar;
    mycar.honk();
    std::cout << mycar.brand + " " + mycar.model << std::endl;
    GrandChildCar1 mylittlecar;
    std::cout << mylittlecar.brand + " " + mylittlecar.model + " " + mylittlecar.submodel1 << std::endl;
    HybridCar halfcar;
    std::cout << mylittlecar.model + " " + halfcar.submodel1 + " " + halfcar.submodel2 << std::endl;
    
    return 0;
}