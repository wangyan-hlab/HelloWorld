#include <iostream>

int main()
{
    try
    {
        int age = 13;
        if (age >= 18)
        {
            std::cout << "Access granted - you're old enough.\n";
        }
        else
        {
            throw age;
        }
    }
    catch (int myNum)
    {
        std::cout << "Access denied - Your must be at least 18 years old.\n";
        std::cout << "Age is " << myNum << std::endl;
    }
}
