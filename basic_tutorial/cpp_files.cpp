#include <iostream>
#include <fstream>

int main()
{
    std::ofstream MyFile("filename.txt");
    MyFile << "Hello world!\nFiles can be tricky, but it is fun enough!";
    MyFile.close();

    std::string  myText;
    std::ifstream MyReadFile("filename.txt");
    while (getline (MyReadFile, myText))
    {
        std::cout << myText << std::endl;
    }
    MyReadFile.close();
}