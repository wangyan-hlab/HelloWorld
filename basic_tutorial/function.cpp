#include <iostream>

void myFunction(std::string text="Gosh!")
{
    std::cout << text << std::endl;
}

int addFunction(int a, int b)
{
    return a + b;
}

void arrayFunction(int myNumber[5])
{
    for (int i=0;i<5;i++)
    {
        std::cout << myNumber[i] << "\n";
    }
}

int sum(int k) {
  if (k > 0) {
    return k + sum(k - 1);
  } else {
    return 0;
  }
}

int main()
{
    for (int i=0;i<4;i++)
    {
        myFunction();
    }
    myFunction("I just got executed!");

    int a = 2;
    int b = 10;
    int c = addFunction(a, b);
    std::cout << std::to_string(c) + " is a perfect number." << std::endl;

    int myNumbers[5] = {12,23,34,45,56};
    arrayFunction(myNumbers);

    int result = sum(10);
    std::cout << "1+2+...+9+10 = " << result << std::endl;

    return 0;
}