#include <iostream>
#include <iomanip>

int main()
{   
    /*Multi-line comments
      Multi-line comments
      Multi-line comments*/
    std::cout << "Hello world!" << std::endl;
    std::cout << "Hello world again!" << std::endl;

    int a;
    double b;
    int c;
    int* addr_a;
    double* addr_b; 
    int* addr_c; 
    a = 10;
    b = 20.056753;
    c = 30;
    addr_a = &a;
    addr_b = &b;
    addr_c = &c;

    std::cout << std::fixed << std::setprecision(6) << a+b <<std::endl;

    std::cout << std::fixed << std::setprecision(5) << 12.23444413432432 << std::endl;

    std::cout << "a's address:" << addr_a << std::endl;
    std::cout << "b's address:" << addr_b << std::endl;
    std::cout << "c's address:" << addr_c << std::endl;

    std::cout << "addr_a's address:" << &addr_a << std::endl;
    std::cout << "addr_b's address:" << &addr_b << std::endl;
    std::cout << "addr_c's address:" << &addr_c << std::endl;
    
    int d;
    double e;
    int f;
    d = *addr_a;
    e = *addr_b;
    *addr_b = 1.23;
    f = *addr_c;

    std::cout << "d:" << d << std::endl;
    std::cout << std::fixed << std::setprecision(6) << "e:" << e << std::endl;
    std::cout << std::fixed << std::setprecision(6) << "new_b:" << b << std::endl;
    std::cout << "f:" << f << std::endl;

    std::cout << "d's address:" << &d << std::endl;
    std::cout << "e's address:" << &e << std::endl;
    std::cout << "f's address:" << &f << std::endl;

    return 0;
}