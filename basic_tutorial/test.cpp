#include <iostream>

int main()
{
    std::string food = "Pizza"; // food variable
    std::string &meal = food;   // reference to food

    std::cout << food << std::endl; // Outputs Pizza
    std::cout << meal << std::endl; // Outputs Pizza

    std::cout << &food << "  " << &meal << std::endl;

    return 0;
}